var LAYER_COUNT = 10;
var KSED = function () {
  this.LAYER_COUNT = 10;
  this.scrollSpeedMax = 16;
  this.dt = 0;
  this.input;
};

var LD33 = function () {
  this.scrollSpeed = 4;
  this.scrollSpeedMax = 16;
  this.dt = 0;
  this.input;
};

LD33.prototype.update = function () {
  this.dt = this.getDT();
};

// For setting velocity.
LD33.prototype.pixelsPerSecond = function (px) {
  return px * 60;
}

// For setting acceleration.
LD33.prototype.pixelsPerSecondSquared = function (px) {
  return px * 60 * 60;
};

LD33.prototype.scrollBG = function () {
  bg.body.velocity.x = -ld.pixelsPerSecond(this.scrollSpeed);
  if (bg.position.x <= -400) { bg.position.x += 400 }
};

LD33.prototype.getDT = function () {
  return game.time.elapsed / 1000;
};

LD33.prototype.speedUpScrollSpeed = function (incr) {
  if (!incr) { incr = 1 / 60; }
  if (this.scrollSpeed <= this.scrollSpeedMax) {
    this.scrollSpeed += incr * this.dt;
  }
  else {
    this.scrollSpeed = this.scrollSpeedMax;
  }

};

LD33.prototype.addPlatforms = function () {
  this.platforms = game.add.group();
  for (var i = 0; i < 2; i++) {
    var platform;
    platform = game.add.sprite(400 * i, game.world.height, 'road_plat');
    platform.anchor.y = 1;
    game.physics.enable(platform, Phaser.Physics.ARCADE);
    platform.body.allowGravity = false;
    platform.body.immovable = true;
    platform.visible = false;
    this.platforms.add(platform);
  }
};

LD33.prototype.changeScaleMode = function () {
  // game.stage.scale.startFullScreen();
  if (game.scale.currentScaleMode == Phaser.ScaleManager.SHOW_ALL) {
    game.scale.scaleMode = Phaser.ScaleManager.RESIZE
  }
  else if (game.scale.currentScaleMode == Phaser.ScaleManager.RESIZE) {
    game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT
  }
  else if (game.scale.currentScaleMode == Phaser.ScaleManager.EXACT_FIT) {
    game.scale.scaleMode = Phaser.ScaleManager.NONE
  }
  else {
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL
  }

  game.scale.fullScreenScaleMode = game.scale.scaleMode;

};

LD33.prototype.goFullscreen = function () {
  // game.stage.scale.startFullScreen();
  game.scale.startFullScreen(false);
  //this.changeScaleMode();
};

var initLayers = function () {
  for (var i = 1; i < ksed.LAYER_COUNT; i++) {
    ksed['layer' + i] = game.add.group();
  }
};

var addSpriteToLayer = function (spr, layerNum) {
  ksed[Math.min(Math.max(layerNum, 1), ksed.LAYER_COUNT)].add(spr);
};

var ld = new LD33();
var ksed = new KSED();
var recyclone = new Recyclone();
var selected;
//var game = new Phaser.Game(320, 240, Phaser.CANVAS, 'kingdom-stone-ed', { preload: preload, create: create, update: update, render: render });
var game = new Phaser.Game(1280, 720, Phaser.CANVAS, 'recycloned', { preload: preload, create: create, update: update, render: render });

var previousMousePosition = new Phaser.Point(0, 0);
var previousMouseClickPosition = new Phaser.Point(0, 0);
var boxSelectRect = new Phaser.Rectangle(0, 0, 0, 0);
var boxSelectSpr;

recyclone.detectFileAccess();
recyclone.game = game;

function preload() {

  game.load.image('ui', 'img/chaser-ui-154x64.png');
  game.load.image('fx', 'img/chaser-fx-154x64.png');
  game.load.image('sq', 'img/chaser-sq-154x64.png');

  game.load.image('bullet', 'img/white.png');
  game.load.image('warning', 'img/PLACEHOLDER/warning.png');
  game.load.image('barrier', 'img/barrier.png');

  game.load.image('road_plat', 'img/road_platform.png');

  game.load.image('background', 'img/bg/bgidea.png');
  game.load.image('grid32', 'img/gridTile32.png');

  game.load.image('cursor', '../data/steam-1-20-4/images/24701200.png'); // Carol Cursor
    
  // Audio
  game.load.audio('sfxDie', 'sfx/Explosion3.wav');
  game.load.audio('sfxGet', 'sfx/Get.wav');
  game.load.audio('sfxJump', 'sfx/Jump6.wav');
  game.load.audio('sfxBulletHit', 'sfx/Hit_Hurt2.wav');
  game.load.audio('sfxBoom', 'sfx/Hit_Hurt3.wav');
  game.load.audio('sfxBarrier', 'sfx/PlaceBarrier.wav');
  game.load.audio('sfxDamage', 'sfx/Randomize11.wav');

}

var debugMode = false;
var player;
var allySq;
var allyFx;
var enemy;
var facing = 'left';
var jumpTimer = 0;
var cursors;
var btnJump;
var btnJumpAlt;
var btnAttack;
var bg;

var grp_activeObjects;
var mouseMode = 'place';
mouseMode = 'move';
mouseMode = 'moveBackground';

function create() {
  console.log(recyclone);
  game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
  //console.log(recyclone.reloadAOTex);
  game.load.onLoadStart.add(function () { console.log("Beginning to load images for level...") }, this);
  game.load.onLoadComplete.add(recyclone.reloadAOTex, this);
  game.physics.startSystem(Phaser.Physics.ARCADE);

  game.world.setBounds(0, 0, 7680, 4320);
  recyclone.width = game.world.width;
  recyclone.height = game.world.height;
  //game.world.width = 7680;
  //game.world.height = 4320;
  

  bg = game.add.tileSprite(0, 0, game.world.bounds.width * 2,
    game.world.bounds.height, 'grid32');
  game.physics.enable(bg, Phaser.Physics.ARCADE);
  bg.body.allowGravity = false;
  bg.body.immovable = true;
  bg.body.maxVelocity.x = ld.pixelsPerSecond(16);

  game.physics.arcade.gravity.y = 300;

  cursors = game.input.keyboard.createCursorKeys();
  btnJumpAlt = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  btnJump = game.input.keyboard.addKey(Phaser.Keyboard.Z);
  btnAttack = game.input.keyboard.addKey(Phaser.Keyboard.X);

  ld.addPlatforms();
  //ld.input = new Input();
  
  boxSelectSpr = game.add.sprite(0, 0, 'bullet');
  mouseCursor = game.add.sprite(32, 160, 'warning');
  if (game.cache.checkImageKey('cursor')) {
    mouseCursor.loadTexture('cursor');
  }
  //game.camera.follow(mouseCursor, Phaser.Camera.FOLLOW_TOPDOWN);
  
  //game.canvas.addEventListener('mousedown', requestLock);
  //game.physics.enable(mouseCursor, Phaser.Physics.ARCADE);
  
  ksed.input = new Input();
  initLayers();
  grp_activeObjects = game.add.group();

  game.input.mouse.capture = true;

  game.input.onDown.add(function () {
    if (mouseMode == 'place') {
      grp_activeObjects.add(game.add.sprite(game.input.worldX, game.input.worldY, 'bullet'));

      var spawnString = '';
      grp_activeObjects.forEachExists(function (actObj) {
        spawnString += generateCreateObjectAOBxyz(actObj.x, actObj.y, 2);
      }, this);
      //console.log('Current AOB: ' + spawnString);
    }
    else if (mouseMode == 'boxSelect') {
      var mp = game.input.activePointer.position;
      previousMouseClickPosition.set(mp.x, mp.y);
      boxSelectSpr.visible = true;
    }
  });

  // When mouse click is released. Doesn't matter which button or what location.
  game.input.onUp.add(function () {
    if (mouseMode == 'boxSelect' && boxSelectRect) {
      console.log('box Ready');
      mouseMode = 'move';
      recyclone.selectedObjects = [];
      grp_activeObjects.forEachExists(function (actObj) {
        if (boxSelectRect.containsRect(actObj.getBounds())) {
          recyclone.selectedObjects.push(actObj);
        }
      }, this);
      boxSelectRect.setTo(0, 0, 0, 0);
      boxSelectSpr.width = 0;
      boxSelectSpr.height = 0;

      console.log('recyclone.selectedObjects: ' + recyclone.selectedObjects);
      //console.log('Current AOB: ' + spawnString);
    }
    else if (mouseMode == 'move') {
      /*
      var mp = game.input.activePointer.position;
      var pc = previousMouseClickPosition;
      var md = new Phaser.Point(mp.x - pc.x + game.camera.x, mp.y - pc.y + game.camera.y);
      for (var so of recyclone.selectedObjects) {
        so.position.set(so.position.x + md.x, so.position.y + md.y);
      }
      */
    }
    else {
      
    }
  });

  game.input.addMoveCallback(function () {
    var mp = game.input.activePointer.position;
    // Middle Mouse Button.
    if (game.input.activePointer.button == 1) {
      var mdx = mp.x - previousMousePosition.x;
      var mdy = mp.y - previousMousePosition.y;

      game.camera.x -= mdx;
      game.camera.y -= mdy;
      
      // console.log(mp);
      // console.log(mp);
      // console.log(mdx);
    }

    if (mouseMode == 'boxSelect') {
      var pc = previousMouseClickPosition;
      boxSelectRect.setTo(pc.x + game.camera.x, pc.y + game.camera.y, mp.x - pc.x + game.camera.x, mp.y - pc.y + game.camera.y);
    }

    previousMousePosition.set(mp.x, mp.y);
  });
}


function requestLock() {
  game.input.mouse.requestPointerLock();
}

function update() {
  mouseCursor.position.set(game.input.worldX, game.input.worldY);

  if (mouseMode == 'boxSelect' && boxSelectRect) {
    // Draw the selection box.
    var b = boxSelectRect;
    boxSelectSpr.position.set(b.left, b.top);
    boxSelectSpr.width = b.width;
    boxSelectSpr.height = b.height;
  }
  else { boxSelectSpr.visible = false; }

  if (cursors.up.isDown) {
    //player.body.moveUp(300)
    game.camera.y -= 8;
  }
  else if (cursors.down.isDown) {
    //player.body.moveDown(300);
    game.camera.y += 8;
  }

  if (cursors.left.isDown) {
    //player.body.velocity.x = -300;
    game.camera.x -= 8;
  }
  else if (cursors.right.isDown) {
    //player.body.moveRight(300);
    game.camera.x += 8;
  }

  grp_activeObjects.forEachExists(function (actObj) {
    if (actObj.x != actObj.ao.x || actObj.y != actObj.ao.y) {
      actObj.ao.x = Math.floor(actObj.x);
      actObj.ao.y = Math.floor(actObj.y);
      actObj.ao.modified = true;
    }
  });

}

function render() {
  if (debugMode) {
    grp_activeObjects.forEachExists(function (actObj) {
      if (actObj.ao && actObj.ao.addr) {
        game.debug.text("Pos: (" + actObj.x + ", " + actObj.y + "):(" + decToHexDWord(actObj.x) + ", " + decToHexDWord(actObj.y) + ")", actObj.x - game.camera.x, actObj.y - game.camera.y);
        game.debug.text(actObj.ao.addr, actObj.x - game.camera.x, actObj.y - game.camera.y - 16);
      }
    });
  }

  // game.debug.text(game.time.physicsElapsed, 32, 32);
  // game.debug.body(player);
  //game.debug.bodyInfo(player, 16, 24);
  if (player) {
    game.debug.text("NMEE: " + enemy.hp, 16, 24);
    game.debug.text("UILI: " + player.hp, 16, 48);
    game.debug.text("SQLI: " + allySq.hp, 16, 72);
    game.debug.text("FXLI: " + allyFx.hp, 16, 96);
    game.debug.text("SPED: " + ld.scrollSpeed.toFixed(2), 16, 120);
  }

  if (debugMode) {
    game.debug.text("Pos: (" + game.input.worldX + ", " + game.input.worldY + "):(" + decToHexDWord(game.input.worldX) + ", " + decToHexDWord(game.input.worldY) + ")", game.input.worldX - game.camera.x, game.input.worldY - game.camera.y);
    //game.debug.text("Pos: ("+game.input.worldX+", "+game.input.worldY+"):("+decToHexDWord(game.input.worldX)+", "+decToHexDWord(game.input.worldY)+")", game.input.worldX, game.input.worldY);
    
    grp_activeObjects.forEachExists(function (actObj) {
      //game.debug.text("Pos: ("+actObj.x+", "+actObj.y+"):("+decToHexDWord(actObj.x)+", "+decToHexDWord(actObj.y)+")", actObj.x + game.camera.x, actObj.y + game.camera.y);
      //game.debug.text("Pos: ("+actObj.x+", "+actObj.y+"):("+decToHexDWord(actObj.x)+", "+decToHexDWord(actObj.y)+")", actObj.x - game.camera.x, actObj.y - game.camera.y);
    });
    game.debug.cameraInfo(game.camera, 32, 32);
  }

  if (recyclone.selectedObjects.length > 0) {
    for (var i = 0; i < recyclone.selectedObjects.length; i++) {
      var actObj = recyclone.selectedObjects[i];
      // game.debug.text("Pos: (" + actObj.x + ", " + actObj.y + "):(" + decToHexDWord(actObj.x) + ", " + decToHexDWord(actObj.y) + ")", actObj.x + game.camera.x, actObj.y + game.camera.y);
      game.debug.text("Pos: (" + actObj.x + ", " + actObj.y + "):(" + decToHexDWord(actObj.x) + ", " + decToHexDWord(actObj.y) + ")", actObj.x - game.camera.x, actObj.y - game.camera.y);
    }
  }

}
