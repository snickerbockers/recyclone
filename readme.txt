For Build Testers and Level Designers: 

**FIRST: Run SETUP.BAT and wait for it to complete.**
 (This will copy FP.exe and Assets.dat where they need to be, then start dumping images and level data. If you do not use the default installation directory, feel free to edit this file to the correct path.)
**THEN: You can run the editor from recyclone.exe.**
(*Alternatively editor can run in Google Chrome from /resources/app/editor/index.html:*)  
* Open a level file by pressing O.
* *(You can select the example levels from /exampleLevels or from /resources/app/data/<fp-version-here>/images).*
* Save a modified .lvl file by processing P.
* Change the Editor's fullscreen mode by pressing F.
* Change the editor's scaling mode by pressing S.
* Move the camera around the level with arrow keys or by middle-clicking and dragging.
* Left-Click and drag on objects to move them.
* Left-Click on an object then press L to remap the display image 
* *(You can select the image from /resources/app/data/<fp-version-here>/images).* 
* You currently cannot create new objects, only move what is already there.
--

Instructions from source:

Step one: Clone this repo:
> git clone https://Retl@bitbucket.org/Retl/recyclone.git

Step : Change to the recyclone directory :
> cd recyclone

Step: Install Dependencies: 
> npm install

Step 2: Put a copy of FP.exe and the corresponding Assets.dat in the 'data/orig/' directory.

Step 3: Dump the asset data, then the level data. This order is important to associate the levels with the images used.
> cd tools
> node locateAssetData.js
> node locateLevelData.js
> cd ..

Step 4: Edit a .lvl file in the editor. (You may need to manually remove object entries from the .lvl to prevent getting thousands of incorrect instances on screen.)
The editor can be launched in Google Chrome from <editor/index.html>.
O key opens a file, P saves the file. Arrow keys pan the camera.

Step 5: Save the .lvl file

Step 6: Re-insert the modified level data:
node replaceLevelData.js <pathToLVLFile>


=====================================
== For GNU/Linux users:
====================================

Using this on Linux is largely the same as using it on Windows (with s/FP.exe/Chowdren/g) with the following exceptions:
* You'll need to install radare2 (radare.org).  The `npm install` only installs the r2pipe javascript binding, not radare2 itself
* You'll need to copy bin64/Chowdren into the tools directory
* instead of running locateLevelData.js in the tools directory, you will run locateLevelDataLinux64.js
* The linux exporter is replaceLevelDataLinux64.js in the tools directory.  It works but it hasn't been thoroughly tested so there are probably things that don't work.

--
