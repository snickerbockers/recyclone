var r2pipe = require("r2pipe");
var fs = require('fs');

r2pipe.options = "-w";
var r2 = r2pipe.open("Chowdren");

function write_frame(frame_no) {
  var lvl_path = "../data/unknown/levels/" + frame_no + ".lvl";
  var lvl_frame = JSON.parse(fs.readFileSync(lvl_path));

  for (var idx = 0; idx < lvl_frame.activeObjects.length; idx++) {
    var obj = lvl_frame.activeObjects[idx];

    if (obj.error != 0)
      continue;

    r2.cmd("s " + obj.x_addr);
    r2.cmd("s+ 1");
    wv_cmd = "wv4 "+ obj.x;
    r2.cmd(wv_cmd);

    r2.cmd("s " + obj.y_addr);
    r2.cmd("s+ 1");
    wv_cmd = "wv4 " + obj.y;
    r2.cmd(wv_cmd);
  }
}

var main = function() {
  for (var frame_no = 1; frame_no <= 87; frame_no++) {
    write_frame(frame_no);
  }
  r2.quit();
};


main();
