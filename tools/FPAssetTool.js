var fs = require('fs');

/*
var runningDataLength = 0;
var finalReport = "";

var frameObjectDataOffsets = [];

var asdf;
*/


var assetFileIn = 'Assets.dat';
var assetFileOut = 'Assets.dat.KSED.txt';

var AOB_pngHeader = '89504e470d0a1a0a';
var AOB_firstTurretus = '6a0268b501000068d1040000';

var searchFor = '00ffaa99';
searchFor = '6a757374';
searchFor = '6a0268b501000068d1040000';
//searchFor = '68b5010000';
// 68D1040000
//

searchFor = AOB_pngHeader;

if (process.argv[2]) {
    searchFor = process.argv[2].toLowerCase();
}

var BUFFER_STEP_SIZE = Math.ceil(searchFor.length/2); // Problem: Buffer as a string length will be different than the buffer as a set of bytes, and the buffer-size specified here is indicating actual byte size.
var BUFFER_SIZE = (4 * 1000) + searchFor.length; // In bytes.

// OVERWRITING IT BECAUSE REASONS.
var BUFFER_STEP_SIZE = 32-11;
var BUFFER_SIZE = 32;
var STARTING_OFFSET = 0;

var hexSet;
var hexString;
var filename = 'FP.exe';
var filenameOut = 'fp_turbo.exe'
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
filename = assetFileIn;
filenameOut = assetFileOut;


var segmentOffsetStart = 0;
var segmentOffsetEnd = 0;

var chunkCount = 1;

if (process.argv[3]) {
    filename = process.argv[3];
}

var readStream;
var writeStream;

var listFoundParts = function () {
  console.log('listFoundParts Called.');
  console.log(frameObjectDataOffsets);
  for (var offset of frameObjectDataOffsets) {
    if (Number.isInteger(offset)) {
      fs.open(filename, 'r', function(status, fd) {
        var buffer = new Buffer(BUFFER_SIZE);
        fs.read(fd, buffer, /*BUFFER_SIZE * j*/ 0, BUFFER_SIZE, offset/2, null);
        //Pattern: 12bytes object args, 16 bytes otherstuff? Maybe? Checkthis.
        var buffText = buffer.toString('hex');
        console.log('Data at position ('+ offset/2 +')' + buffText);

      });
    }
  }
};

var main = function () {

    readStream = new fs.ReadStream(filename);
    writeStream = new fs.WriteStream(filenameOut);

    readStream.setEncoding('hex');

    // Not sure what this does.
    readStream.on('open', function (chunk) {
	//console.log(chunk.toString('hex', 0));
	//console.log(chunk);
    });

    // Actually read the file.
    readStream.on('data', function (chunk){
	asdf = chunk;
  console.log('Data event Read.');
  /*
	//console.log(chunk);
	//console.log('To string: ' + asdf.toString());
	//console.log('To hex-string: ' + asdf.toString('hex'));
  if (false) {console.log('To hex-string: ' + asdf.toString('hex'));}
	console.log('Search for ('+ searchFor +') result: ' + asdf.toString().search(searchFor));
	var searchPos = asdf.toString('hex').search(searchFor);
	segmentOffsetStart = searchPos;
	segmentOffsetEnd = segmentOffsetStart + searchFor.length;
	console.log('Search(hexString) for ('+ searchFor +') result: ' + searchPos);
	//console.log('Bytevalue at searchpos: ' +chunk[Math.floor(searchPos/2)].toString(16));
  */
	console.log('Chunk Length: ' + chunk.length);

	console.log('}-----@WRITING CHUNK@-----{');
  var modifiedChunk = chunk.toString('hex');

  /*
	var modifiedChunk = chunk.toString('hex');
	if (searchPos > -1) {
      // var theOffset = runningDataLength + (searchPos/2);
      var theOffset = runningDataLength + searchPos;
      frameObjectDataOffsets.push(theOffset);
	    console.log('Bytematch found at offset ('+ theOffset +'). Replacing with byte 0x20, a blank space...');
	    var replaceWith = '';
	    for (var k = 0; k < Math.floor(searchFor.length/2); k++) {replaceWith += '20';}
	    modifiedChunk = modifiedChunk.replace(searchFor, replaceWith);
	    console.log(modifiedChunk);
	    console.log('Done.');
            finalReport += 'In Chunk#'+ chunkCount +', at position'+ searchPos +', wrote modified chunk: ' + modifiedChunk;
	}
  */

	console.log('Writing chunk to '+ filenameOut +'...');
	//writeStream.write(asdf);
	writeStream.write(modifiedChunk, 'hex');
	//writeStream.write('ffeeddccbbaa99887766554433221100', 'hex');
	console.log('Done.');
	chunkCount++;
	console.log('|-----@NEXT CHUNK@-----|');
  runningDataLength += chunk.length;
    });

    // After getting the entire body.
    readStream.on('end', function () {
    	console.log('END. Final running data length: ' + runningDataLength);
    	//console.log(finalReport);
    	writeStream.end();
      //listFoundParts();
    });

    readStream.on('error', function () {
	console.log('@@@AN ERROR OCCURED@@@');
    });

    function displaySegmentContents () {
	if (typeof segmentOffsetStart !== 'undefined' && typeof segmentOffsetEnd !== 'undefined') {
	    // g2g now
	    fs.read(filename, 'hex');
	}
	else
	{
	    console.log('Unable to display segment content: At least one of the offsets were undefined.');
	}
    }
};

main();
//listFoundParts();
