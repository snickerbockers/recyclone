var fs = require('fs');
var r2pipe = require("r2pipe");

/*
 * RELEVENT RADARE2 COMMANDS
 *
 * is                 - list all symbols
 * s sym.<symbol>     - seek to symbol
 * s <nothing>        - print position in executable file
 * pa <inst>          - assemble instruction
 * pi <count>         - print the next <count> instructions
 * so <count>         - seek forward <count> instructions
 */

var r2 = r2pipe.open("Chowdren");

/*
 * From what I've seen, chowdren is mechanical enough to always move immediate
 * values into the 32-bit versions of di and si when it calls get_internal_image
 * or create_%s_%d functions.  In theory it could do more complicated stuff than
 * that, but in practice it doesn't.
 */
var mov_edi_re = /mov edi, (0x[0-9a-fA-F]+|[0-9]+)/;
var mov_esi_re = /mov esi, (0x[0-9a-fA-F]+|[0-9]+)/;
var call_get_internal_image = r2.cmd("pa call sym.get_internal_image");

// TODO: does this pick up the function calls that don't get demangled by r2?
var call_create_func = /call sym\.create_([a-zA-Z0-9]+_[0-9]+)/;

var set_width_re = /mov dword \[rdi \+ 8\], (0x[0-9a-fA-f]+)/;
var set_height_re = /mov dword \[rdi \+ 0x10\], (0x[0-9a-fA-f]+)/;

/* 
 * iterate throught the elf symbols and come up with a map
 * of all object classes and the addresses and names of the functions
 * that create them.
 */
function list_object_classes() {
  var obj_classes = {};

  // dump the symbol table and search for create_foo_%d functions
  var symbols = r2.cmd("is | grep -E \"create_[a-zA-Z0-9]+_[0-9]+\"");

  // process symbol fields for each symbol
  symbols = symbols.match(/[^\r\n]+/g);
  for (var idx = 0; idx < symbols.length; idx++) {
    var cols = symbols[idx].match(/[^ \t]+/g);
    var vaddr = cols[0].match(/[^=]+/g)[1];
    var paddr = cols[1].match(/[^=]+/g)[1];
    var create_func = cols[7].match(/[^=]+/g)[1];
    var func_len = cols[4].match(/[^=]+/g)[1];
    var class_name = "";
    if (create_func.substring(0, 17) == "_GLOBAL__sub_I__Z") {
      // radare2 doesn't demangle the symbols where bind=LOCAL, IDK why
      class_name = create_func.substring(26, create_func.length - 2);
    } else {
      class_name = create_func.substring(7);
    }

    var obj_class = {
      "name" : class_name,
      "func" : create_func,
      "vaddr" : parseInt(vaddr, 16),
      "paddr" : parseInt(paddr, 16),
      "func_len" : parseInt(func_len, 10)
    };
    obj_classes[obj_class["name"]] = obj_class;
  }

  return obj_classes;
}

/*
 * seeks to the create function and returns the image id sent to
 * the first get_internal_image call.  This will return null if
 * it doesn't find an image id.
 */
function find_first_image(obj_class) {
  // seek to the object's create function
  r2.cmd("s sym." + obj_class['func']);

  // address after the end of the function
  var end_address = parseInt(r2.cmd("s"), 16) + obj_class['func_len'];

  var img_id = undefined;

  /*
   * go forward and find the last mov into %edi before the first
   * get_internal_image call.  This will fuck up if Chowdren uses
   * any other opcode to write to %edi, but it doesn't look like
   * it ever does.
   */
  while (parseInt(r2.cmd("s"), 16) < end_address) {
    var inst = r2.cmd("pi 1");

    var re_match = mov_edi_re.exec(inst);
    if (re_match != null){
      img_id = re_match[1];
    } else if (inst.match(/call sym.get_internal_image/)) {
      if (img_id != undefined) {
        return parseInt(img_id);
      } else {
        console.log("ERROR: Unable to find get_internal_image parameter in %s", obj_class['func']);
      }
    }
    // seek to next instruction
    r2.cmd("so 1");
  }
  return null;
}

/*
 * seek to the given frame and return a list of all the objects it
 * instantiates.
 */
function parse_frame(frame_no) {
  frame_init_func = "Frames::on_frame_" + frame_no + "_init";
  r2.cmd("s sym." + frame_init_func);

  /*
   *TODO: I'm not sure if there are any special characters in function names
   * that grep might interpret as regexp special characters
   */
  var frame_sym = r2.cmd("is | grep "+frame_init_func+'$').match(/[^ \t]+/g);
  var func_len = parseInt(frame_sym[4].match(/[^=]+/g)[1]);
  var end_address = parseInt(r2.cmd("s"), 16) + func_len;

  var last_x_val = undefined;
  var last_y_val = undefined;
  var frame_w = undefined;
  var frame_h = undefined;
  var last_x_val_addr = undefined;
  var last_y_val_addr = undefined;

  var objs = [];

  var obj_classes = list_object_classes();

  while (parseInt(r2.cmd("s"), 16) < end_address) {
    var inst = r2.cmd("pi 1");

    var re_match = set_width_re.exec(inst);
    if (re_match != null)
      frame_w = re_match[1];

    re_match = set_height_re.exec(inst);
    if (re_match != null)
      frame_h = re_match[1];

    re_match = call_create_func.exec(inst);
    if (re_match != null) {
      var new_obj = {
        'obj_class' : re_match[1],
        'x' : last_x_val,
        'y' : last_y_val,
        'x_addr' : last_x_val_addr,
        'y_addr' : last_y_val_addr,
        'images' : [],
        'error' : 0
      };

      /*
       * the error flag is set if we can't find the x-pos and y-pos.
       * This tells the replaceLevelDataLinux64.js script to ignore
       * this object so we don't end up corruption stuff.
       */
      if (last_x_val == undefined || last_y_val == undefined)
        new_obj['error'] = 1;
      last_x_val = undefined;
      last_y_val = undefined;
      objs.push(new_obj);
    }
    re_match = mov_edi_re.exec(inst);
    if (re_match != null) {
      last_x_val = parseInt(re_match[1]);
      last_x_val_addr = parseInt(r2.cmd("s"));
      if (last_x_val > 0x7fffffff)
        last_x_val -= 0x100000000;
    }

    re_match = mov_esi_re.exec(inst);
    if (re_match != null) {
      last_y_val = parseInt(re_match[1]);
      last_y_val_addr = parseInt(r2.cmd("s"));
      if (last_y_val > 0x7fffffff)
        last_y_val -= 0x100000000;
    }

    r2.cmd("so 1");
  }

  for (var i = 0; i < objs.length; i++) {
    var img_id = find_first_image(obj_classes[objs[i]['obj_class']]);
    if (img_id != null)
      objs[i]['image'] = img_id;
  }

  return {
    'width' : parseInt(frame_w),
    'height' : parseInt(frame_h),
    "fpVersion" : "unknown",
    "fpSize" : 0,
    "levelId" : frame_no,
    'activeObjects' : objs
  };
}

var main = function() {
  for (var frame_no = 1; frame_no <= 87; frame_no++) {
    frameJson = parse_frame(frame_no);
    lvlPath = "../data/unknown/levels/" + frame_no + ".lvl";
    console.log("dumping %s...", lvlPath);
    fs.writeFileSync(lvlPath, JSON.stringify(frameJson, null, 2));
  }

  r2.quit();
};

main();
