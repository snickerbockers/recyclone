var fs = require('fs');
var bytetools = require('../editor/js/bytetools.js');
/*var readline = require('readline');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
*/

var runningDataLength = 0;
var finalReport = "";

/*
if (process.argv[2]) {
    var searchFor = process.argv[2].toLowerCase();
}
else
{
  var searchFor = /6a[\da-f]{2}68[\da-f]{8}68[\da-f]{8}e8[\da-f]{8}/g;
}*/

var searchFor = /6a[\da-f]{2}68[\da-f]{8}68[\da-f]{8}e8[\da-f]{8}/g;

var BUFFER_STEP_SIZE = Math.ceil(searchFor.length / 2); // Problem: Buffer as a string length will be different than the buffer as a set of bytes, and the buffer-size specified here is indicating actual byte size.
var BUFFER_SIZE = (4 * 1000) + searchFor.length; // In bytes.

// OVERWRITING IT BECAUSE REASONS.
var BUFFER_STEP_SIZE = 32 - 11;
var BUFFER_SIZE = 32;
var STARTING_OFFSET = 0;

var hexSet;
var hexString;

var filenameFP = 'FP.exe';
var filenameOut = 'fp_recyclone.exe'
var filenameLevel = '../data/fp-steam-1.20.4.lvl';
var levelFolder = '../data/steam-1-20-4/mod_levels/';
filenameLevel = '../data/FP-steam-1-20-4-Annotated_LolIMovedAThing.lvl';

var levelData = '';
var levelJSON = null;

var fullHexData = '';

var segmentOffsetStart = 0;
var segmentOffsetEnd = 0;

var chunkCount = 1;

// Configuration options for this insertion run.
var randomize = false;
var allowLevelSizeChange = true;

if (process.argv[2]) {
  if (process.argv[2].toLowerCase() == 'steam-1-20-4') {
    levelFolder = '../data/steam-1-20-4/mod_levels/';
  }
  else if (process.argv[2].toLowerCase() == 'steam-1-20-4-random') {
    levelFolder = '../data/steam-1-20-4/mod_levels/';
    randomize = true;
  }
  else if (process.argv[2].toLowerCase() == 'steam-1-20-6'
    || process.argv[2].toLowerCase() == 'steam-1-21-4') {
    levelFolder = '../data/steam-1-21-4/mod_levels/';
  }
  else if (process.argv[2].toLowerCase() == 'unknown') {
    levelFolder = '../data/unknown/mod_levels/';
  }
  else {
    levelFolder = null;
    filenameLevel = process.argv[2];
  }
}

var readStream;
var readStreamLevel;
var writeStream;

var progressString = '';

function getUpdatedLevelSizeAoB (lvlObj, data, lastIndex) {
  if (typeof lvlObj.widthIndex == 'undefined' || 
      typeof lvlObj.heightIndex == 'undefined' || 
      typeof lvlObj.virtualWidthIndex == 'undefined' || 
      typeof lvlObj.virtualHeightIndex == 'undefined' ) {
      return null;
  }
      
  var AoBLevelSize = "";
  var widthReplacement = bytetools.decToHexInt(lvlObj.width);
  var heightReplacement = bytetools.decToHexInt(lvlObj.height);
  verboseLog("lastIndex: " + lastIndex);
  verboseLog(widthReplacement);
  verboseLog(heightReplacement);
  AoBLevelSize += data.substring(lastIndex, lvlObj.widthIndex) + widthReplacement;
  lastIndex = AoBLevelSize.length;
  AoBLevelSize += data.substring(lastIndex, lvlObj.heightIndex) + heightReplacement;
  lastIndex = AoBLevelSize.length;
  AoBLevelSize += data.substring(lastIndex, lvlObj.virtualWidthIndex) + widthReplacement;
  lastIndex = AoBLevelSize.length; 
  AoBLevelSize += data.substring(lastIndex, lvlObj.virtualHeightIndex) + heightReplacement;
  
  verboseLog("lvlObj.heightIndex: " + lvlObj.heightIndex);
  verboseLog("lvlObj.widthIndex: " + lvlObj.widthIndex);
  verboseLog("lvlObj.virtualHeightIndex: " + lvlObj.virtualHeightIndex);
  verboseLog("lvlObj.virtualWidthIndex: " + lvlObj.virtualWidthIndex);
  
  verboseLog("Original Size AoB: " + data.substring(lvlObj.widthIndex, lvlObj.virtualHeightIndex + heightReplacement.length));
  verboseLog("New Size AoB: " + AoBLevelSize.substring(lvlObj.widthIndex, lvlObj.virtualHeightIndex + heightReplacement.length)); 
  
  verboseLog("Original Size AoB: " + data.substring(0, lvlObj.widthIndex + 70));
  verboseLog("New Size AoB: " + AoBLevelSize.substring(0, lvlObj.widthIndex + 70));
  return AoBLevelSize;
};

function verboseLog(str) {
  if (false) {
    console.log(str);
  }
}

function reportElapsedTime () {
  var dt = new Date() - startTime;
  var dts = (dt/1000);
  var sec = Math.floor(dts % 60);
  var min = Math.floor(dts/60) % 60;
  var h = Math.floor(dts/3600);
  return (h + 'h ' 
  + min 
  + 'm ' + sec + 's');
};

var main = function () {
  var arrLevelFilenames = [];

  // If the user uses version codes on the prompt, get all files based on the folder.
  if (levelFolder) {
    console.log('Level Folder: ' + levelFolder);
    var arrFilenames = fs.readdirSync(levelFolder);
    for (var filename of arrFilenames) {
      if (filename.toLowerCase().search('.lvl') > -1) {
        arrLevelFilenames.push(levelFolder + filename);
      }
    };
  }
  else {
    // Otherwise, default to a single example file.
    arrLevelFilenames = [filenameLevel];
  }
  
  // Read the game binary data.
  console.log('Game Filename: ' + filenameFP);
  var fullHexData = fs.readFileSync(filenameFP, { encoding: 'hex' });
  console.log('FP.exe read complete. Final running data length: ' + (fullHexData.length / 2));
  console.log('Modified game fill will write to: ' + filenameOut);
  if (randomize) {console.log('!!! WARNING: LEVEL RANDOMIZER ACTIVE !!!');}
  
  // Read each Level File, parse the JSON, modify the game.
  for (var lvlFile of arrLevelFilenames) {
    console.log('Reading Level: ' + lvlFile);
    levelData = fs.readFileSync(lvlFile);
    levelJSON = JSON.parse(levelData);
    
    // If randomizer is on, randomize the level Width and Height data.
    if (randomize && levelJSON.width && levelJSON.height) {
        var baseWidth = levelJSON.width;
        var baseHeight = levelJSON.height;
        // New size will be between 0.5 and 1.5 original level on each dimension.
        var randWidth = Math.floor((Math.random() * baseWidth) + (baseWidth/2));
        var randHeight = Math.floor((Math.random() * baseHeight) + (baseHeight/2));
        
        levelJSON.width = randWidth;
        levelJSON.height = randHeight;
      }
    
    // Iterate through all of the objects loaded from the level file, and update the AoB to be inserted later.
    for (var activeObject of levelJSON.activeObjects) {
      
      // If randomizer is on, use random position values to be inserted. 
      if (randomize && levelJSON.width && levelJSON.height) {
        activeObject.x = 32 + Math.floor(Math.random() * (levelJSON.width - 32));
        activeObject.y = 32 + Math.floor(Math.random() * (levelJSON.height - 32));
        // activeObject.layer = 1 + Math.floor(Math.random() * 6);
      }
      // Do the math and update the AoB for the objects.
      var newAoB = bytetools.generateCreateObjectAOBFromAO(activeObject);
      var offsetInt;
      if (!activeObject.addrInt) {
        offsetInt = Number.parseInt(activeObject.addr.substring(2,
          activeObject.addr.length), 16);
        activeObject.addrInt = offsetInt;
      }
      else {
        offsetInt = activeObject.addrInt;
      }
      activeObject.newAoB = newAoB;
    }
    
    // Modifying the game & Injecting new AoBs.
    var fullModData = '';
    var lastIndex = 0;
    
    // Replace the level Width and Height data.
    if (allowLevelSizeChange) {
      var tmp = getUpdatedLevelSizeAoB(levelJSON, fullHexData, lastIndex);
      if (tmp != null)  {
        fullModData = tmp;
        lastIndex = fullModData.length;
      }
      else
      {
        console.log("Attempted to replace level Width and Height but something went wrong. Continuing.");
      }
    }
    
    for (var index in levelJSON.activeObjects) {
      if (levelJSON.activeObjects[index]) {
        //rl.clearLine();
        progressString = 'Updating object in memory: (' + index + '/' + levelJSON.activeObjects.length + ')';
        progressString += ' Time since launch: ' + reportElapsedTime();
        verboseLog(progressString);
        //rl.write(progressString);
        verboseLog("Attempting to update objectSpawn hex at addr {" + levelJSON.activeObjects[index].addr + "}");
        var o = levelJSON.activeObjects[index].addrInt * 2;
        var replacement = levelJSON.activeObjects[index].newAoB;
        //var replacement = activeObject.newAoB + 'e8' + activeObject.addrCreateFun.substring(2,10);
        fullModData += fullHexData.substring(lastIndex, o) + replacement;
        lastIndex = fullModData.length;
      }
    }
    if (lastIndex < fullHexData.length) {
      fullModData += fullHexData.substring(lastIndex);
    }
    verboseLog(fullModData.length + '/' + fullHexData.length);
    if (fullModData.length == fullHexData.length) {
      console.log('Level Mod OK!');
    }
    else {
      throw 'Modded level data and original level data have mismatched size. Email Dazzle@risingslash.net with the steps that led to this error.';
    }
    fullHexData = fullModData;
  };
  
  // Finally, write the modified game.
  console.log('Writing modified executable to ' + filenameOut);
  fs.writeFileSync(filenameOut, fullModData, { encoding: 'hex' });
  console.log('Done in ' + reportElapsedTime() + '.');
  var goodLuck = '!!! LEVEL RANDOMIZER ACTIVE !!! GOOD LUCK, CHASER !!!';
  var warningWall = new Array(goodLuck.length).fill('!').join('');
  if (randomize) {
    console.log('');
    console.log('');
    console.log('');
    console.log(warningWall);
    console.log(goodLuck);
    console.log(warningWall);
    console.log('');
    console.log('');
    console.log('');
    }
};

function createPath(path) {
  try {
    fs.accessSync(path, fs.F_OK);
    // Do something
  } catch (e) {
    console.log('Could not find path "' + path + '". Attempting to create it.');
    fs.mkdirSync(path);
  }
};

var startTime = new Date();
main();
