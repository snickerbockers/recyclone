var fs = require('fs');
var bytetools = require('../editor/js/bytetools.js');
var leveldata = require('./levelData.js');

var runningDataLength = 0;
var finalReport = "";

var frameObjectDataOffsets = [];
var activeObjectMatches = [];

var imageAssets;
var imagesDone = false;
var activeObjectsDone = false;

var AOB_pngHeader = '89504e470d0a1a0a';
var AOB_firstTurretus = '6a0268b501000068d1040000';
var AOB_getInternalImage = '558bec568b7508833cb5a02d03020075';
var AOB_getInternalImage = '558bec568b7508833cb5a02d03020075';

var searchFor = /6a[0-9a-fA-F]{2}68[0-9a-fA-F]{8}68[0-9a-fA-F]{8}e8[0-9a-fA-F]{8}/g;
var AOB_WildHex = '[0-9a-fA-F]{2,50}';
var AOB_WildHexHUGE = '[0-9a-fA-F]{2,200}';
var AOB_WildHexTiny = '[0-9a-fA-F]{2,10}';
var AOB_WildHexShort = '[0-9a-fA-F]{4}';
var AOB_WildHexInt = '[0-9a-fA-F]{8}';
var AOB_callRel32 = /e8[0-9a-fA-F]{8}/g;
var AOB_getInternalImage = /68[\da-fa]{8}c6[\da-f]{12}e8[\da-f]{8}/g;
var AOB_FrameInit = new RegExp('(' + AOB_WildHexInt + ')c7' + AOB_WildHex + '(' + AOB_WildHexInt + ')c7' + AOB_WildHex + '(' + AOB_WildHexInt + ')' + AOB_WildHex + 'c7' + AOB_WildHex + '\\2' + AOB_WildHex + 'c7' + AOB_WildHex + '\\3', 'g');
AOB_FrameInit = new RegExp('c7' + AOB_WildHexShort + '(' + AOB_WildHexInt + ')c7' + AOB_WildHexShort + '(' + AOB_WildHexInt + ')c7' + AOB_WildHexShort + '(' + AOB_WildHexInt + ')' + AOB_WildHexTiny + 'c7' + AOB_WildHex + '\\2' + AOB_WildHexTiny + 'c7' + AOB_WildHex + '\\3', 'g');
AOB_FrameInit = new RegExp('c7' + AOB_WildHexShort + '(' + AOB_WildHexInt + ')c7' + AOB_WildHexShort + '(' + AOB_WildHexInt + ')c7' + AOB_WildHexShort + '(' + AOB_WildHexInt + ')' + AOB_WildHexHUGE + '\\2' + AOB_WildHexHUGE + '\\3', 'g');
//AOB_FrameInit = /c7[\da-f]{12}c7[\da-f]{12}c7[\da-f]{12}/g
var AOB_FrameEnd = /(8b[\da-f]{4}c6[\da-f]{8}00008b[\da-f]{4}c7[\da-f]{10}00000000){10,}/;
var AOB_EndOfFunction = /c3cccc/;


var AOB_FrameInitDV = /8b[\da-f]{4}c7[\da-f]{4}140000008b[\da-f]{4}c7[\da-f]{4}e03900008b[\da-f]{4}c7[\da-f]{4}480700008b[\da-f]{4}c7[\da-f]{4}e03900008b[\da-f]{4}c7[\da-f]{4}48070000/g;
AOB_FrameInitDV = /[\da-f]{6,36}140000008b[\da-f]{6,36}e03900008b[\da-f]{6,36}480700008b[\da-f]{6,36}e03900008b[\da-f]{6,36}48070000/g;
var AOB_FrameInitDVAlt = /14000000[da-f]{4,64}e0390000[da-f]{4,64}48070000[da-f]{4,64}e0390000[da-f]{4,64}48070000[da-f]{4,64}/g;
AOB_FrameInitDVAlt = /14000000[da-f]{4,64}e0390000[da-f]{4,64}48070000[da-f]{4,64}/g;
AOB_FrameInitDVAlt = /c7461814000000c74608e0390000c7460c48070000/g
//AOB_FrameInit = AOB_FrameInitDVAlt;


//var genericFrameInit = 14000000c74608e0390000c7460c480700006a00c74610e03900006a00c7461448070000

var addrGetInternalImage = null;

var BUFFER_STEP_SIZE = Math.ceil(searchFor.length / 2); // Problem: Buffer as a string length will be different than the buffer as a set of bytes, and the buffer-size specified here is indicating actual byte size.
var BUFFER_SIZE = (4 * 1000) + searchFor.length; // In bytes.

// OVERWRITING IT BECAUSE REASONS.
var BUFFER_STEP_SIZE = 32 - 11;
var BUFFER_SIZE = 32;
var STARTING_OFFSET = 0;

var hexSet;
var hexString;

var fullData = '';
var fullImgData = '';

var pathToData = '../data/';
var pathLevels = 'levels/';
var lvlExt = '.lvl';

var filename = 'FP.exe';
if (process.argv[2]) {
  filename = process.argv[2];
}
// var filenameOut = 'fp_turbo.exe'
var fileVersion = "unknown";
var stats = fs.statSync(filename);
var fileSizeInBytes = stats["size"];
console.log("Filezie is: " + fileSizeInBytes);
if (fileSizeInBytes == 31688192) { fileVersion = "steam-1-20-4"; }

console.log(fileVersion);
var filenameOut = '../data/FP-' + fileVersion + '.lvl';
var filenameImg = '../data/Assets-' + fileVersion + '.imgs';

var segmentOffsetStart = 0;
var segmentOffsetEnd = 0;

var chunkCount = 1;

var readStream;
var readImgStream;
var writeStream;

//Steam-1-20-4
var offsetToLoadImage = "0019CDC0";
offsetToLoadImage = "0040CDC0";
//var indexToLoadImage = bytetools.hexIntToInt(offsetToLoadImage, false, true) * 2;
var indexToLoadImage = (1688000 * 2); //0x0019C1C0

var main = function () {

  readStream = new fs.ReadStream(filename);
  readImgStream = new fs.ReadStream(filenameImg);
  writeStream = new fs.WriteStream(filenameOut);

  readImgStream.on('open', function () {

  }).on('data', function (chunk) {
    fullImgData += chunk;
  }).on('end', function () {
    imageAssets = JSON.parse(fullImgData).images;
    //console.log(imageAssets);
    imagesDone = true;
    // mapLevelDataToImages();
  });


  readStream.setEncoding('hex');
  
  // Not sure what this does.
  readStream.on('open', function (chunk) {
  });
  
  // Actually read the file.
  readStream.on('data', function (chunk) {
    fullData += chunk;
  });

  // After getting the entire body.
  readStream.on('end', function () {

    console.log('END. Final running data length in Bytes: ' + (fullData.length / 2));
    var indlocthingy = (fullData.search(AOB_getInternalImage));
    var indlocthingyaddr = bytetools.decToHexInt(indlocthingy/2, false, true);
    console.log("Index of Internal Image thingy for 1-20-4: " + indlocthingy);
    console.log("Addr of that thingy: " + indlocthingyaddr)
    //console.log(finalReport);
    var levelInits = findAllFrameInits(fullData);
    console.log('levelInits.length: ' + levelInits.length);

    for (var lvl of levelInits) {
      var lvlActiveObjects = findAllActiveObjects(lvl.frameAOB, lvl.frameInitIndex);
      console.log('lvlActiveObjects.length: ' + lvlActiveObjects.length);
      lvlActiveObjects = mapLevelDataToImages(lvlActiveObjects);

      var lvlJSON = {};
      lvlJSON.fpVersion = fileVersion;
      lvlJSON.fpSize = runningDataLength;
      lvlJSON.levelId = lvl.frameIndexId;
      lvlJSON.width = lvl.width;
      lvlJSON.height = lvl.height;
      lvlJSON.widthIndex = lvl.widthIndex;
      lvlJSON.heightIndex = lvl.heightIndex;
      lvlJSON.virtualWidthIndex = lvl.virtualWidthIndex;
      lvlJSON.virtualHeightIndex = lvl.virtualHeightIndex;
      lvlJSON.activeObjects = lvlActiveObjects;

      createPath(pathToData);
      createPath(pathToData + fileVersion);
      createPath(pathToData + fileVersion + '/' + pathLevels);
      var lvlFilePath = pathToData + fileVersion + '/' + pathLevels + lvl.frameIndexId + lvlExt;
      //fs.writeFile(lvlFilePath, JSON.stringify(lvlJSON, null, 2), function () { console.log(lvlFilePath); });
      fs.writeFileSync(lvlFilePath, JSON.stringify(lvlJSON, null, 2));
      activeObjectMatches = [];
      lvlActiveObjects = null;
    }

    activeObjectsDone = true;
    // mapLevelDataToImages();

    // var jsonOut = {};
    // jsonOut.fpVersion = fileVersion;
    // jsonOut.fpSize = runningDataLength;
    // jsonOut.activeObjects = activeObjectMatches;

    //writeStream.write(JSON.stringify(jsonOut, null, 2), 'utf8');
    writeStream.end();
  });

  readStream.on('error', function () {
    console.log('@@@AN ERROR OCCURED@@@');
  });

  function displaySegmentContents() {
    if (typeof segmentOffsetStart !== 'undefined' && typeof segmentOffsetEnd !== 'undefined') {
      // g2g now
      fs.read(filename, 'hex');
    }
    else {
      console.log('Unable to display segment content: At least one of the offsets were undefined.');
    }
  }
};

function findAllActiveObjects(lvlAob, lvlAobInd) {
  if (typeof lvlAobInd == 'undefined') {lvlAobInd = 0;}
  var objs = [];
  var re = searchFor;
  var match = [];
  activeObjectMatches = [];
  //console.log('lvlAob: ' + lvlAob);
  while ((match = re.exec(lvlAob)) != null) {
    var hx = match[0].substring(16, 24);
    var hy = match[0].substring(6, 14);
    var hl = match[0].substring(2, 4);
    var addrIntStart = (match.index + lvlAobInd) / 2;
    var addrIntEnd = addrIntStart + (match[0].length / 2);
    var createFun = match[0].substring(26, 34);
    var createFunIntOffsetRelative = bytetools.hexIntToInt(createFun, true);
    var createFunIntOffset = (addrIntEnd + createFunIntOffsetRelative);
        
    // TODO: Fix this and replaceLevelData to use littleEndian, CE, and Int addrs/offsets.
    var ao = {
      'addr': "0x" + bytetools.decToHexDWord((match.index / 2)),
      'addrCreateFun': createFun,
      'addrCreateFunCE': "0x" + bytetools.sliceIntoBytes(createFun),
      'addrInt': addrIntStart,
      'addrIntEnd': addrIntEnd,
      'addrIntCreateFunRelative': createFunIntOffsetRelative,
      'addrIntCreateFun': createFunIntOffset,
      'aob': match[0],
      'x': bytetools.hexIntToInt(hx),
      'y': bytetools.hexIntToInt(hy),
      'layer': bytetools.hexIntToInt(hl)
    };
    findObjectInternalImageId(ao);
    activeObjectMatches.push(ao);
    objs.push(ao)
  }
  return objs;
}

function findObjectInternalImageId(ao) {
  var imageCallSearchStartIndex = (ao.addrIntCreateFun) * 2;
  var imageCallSearchEndIndex = imageCallSearchStartIndex + (fullData.substring(imageCallSearchStartIndex).search(AOB_EndOfFunction));
  var createFuncAoB = fullData.substring(imageCallSearchStartIndex, imageCallSearchEndIndex + 2);
  // console.log('imageCall Index start|end|length: ' + imageCallSearchStartIndex + ' | ' + imageCallSearchEndIndex + ' | ' + (imageCallSearchEndIndex - imageCallSearchStartIndex));

  var calls = findAllCallRel32InString(createFuncAoB, imageCallSearchStartIndex);
  // console.log('findObjectInternalImageId#calls.length ' + calls.length );
  if (calls.length > 0) {
    var indexOfImageLoads = calls[calls.length - 1].targetIndex;
    var indexOfImageLoadsEnd = indexOfImageLoads + (fullData.substring(indexOfImageLoads).search(AOB_EndOfFunction));
    var imageLoadsAoB = fullData.substring(indexOfImageLoads, indexOfImageLoadsEnd + 2);

    var aoImageIds = findAllGetInternalImageCalls(imageLoadsAoB, indexOfImageLoads);
    ao.images = [];
    aoImageIds.map(function (val, ind, arr) { ao.images[ind] = aoImageIds[ind].imgId });
  }
}

function findAllCallRel32InString(str, strIndex) {
  var re = AOB_callRel32;
  var match = [];
  var calls = [];
  while ((match = re.exec(str)) != null) {
    var call = getCallRel32Dest(match[0], match.index + strIndex);
    calls.push(call);
  }
  // console.log(str); // DELETEME
  // console.log('Num relCalls found: ' + calls.length);
  return calls;
}


function getCallRel32Dest(callRelStr, callRelIndex) {
  var targetIndex = 0;
  var targetAddrInt = 0;
  var targetAddr = 0;

  var endPos = (callRelIndex + callRelStr.length);
  var relOffset = bytetools.hexIntToInt(callRelStr.substring(2), true);
  targetIndex = endPos + (relOffset * 2);

  targetAddrInt = targetIndex / 2;
  targetAddr = bytetools.decToHexInt(targetAddrInt);

  return ({
    'callRelStr': callRelStr,
    'callRelIndex': callRelIndex,
    'targetIndex': targetIndex,
    'targetAddrInt': targetAddrInt,
    'targetAddr': targetAddr
  });
}

function findAllFrameInits(str) {
  var re = AOB_FrameInit;
  //console.log(AOB_FrameInit);
  var match = [];
  var frameInits = [];
  var locatedZeroInd = -1;
  while ((match = re.exec(str)) != null) {
    console.log(match.index);
    var frameInit = getInitFrameDetails(match, match.index, locatedZeroInd);
    if (false)
    {
    console.log("~----------~");
    console.log(frameInit);
    console.log("~----------~");
    }
    if (frameInit != null) {
      frameInits.push(frameInit);
      console.log(frameInits.length);
    }
    else {
      console.log('Bad frame.');
    }
  }
  return frameInits;
}

function getInitFrameDetails(match, frameInitInd, locatedZeroInd) {
  
  //var frameInitStr = match[0];
  // AOB_FrameInit = new RegExp('c7' + AOB_WildHexShort + '(' + AOB_WildHexInt + ')c7' + AOB_WildHexShort + '(' + AOB_WildHexInt + ')c7' + AOB_WildHexShort + '(' + AOB_WildHexInt + ')' + AOB_WildHexHUGE + '\\2' + AOB_WildHexHUGE + '\\3', 'g');
  
  var frameIndex = bytetools.hexIntToInt(match[1]);
  var width = bytetools.hexIntToInt(match[2]);
  var height = bytetools.hexIntToInt(match[3]);
  var virtualWidth = width;
  var virtualHeight = height;
  if (frameIndex >= 100) { return null; }
  if (frameIndex == 0) {
    if (width != 640 && height != 640) {
      console.log('Width of Screen 0 is expected to be 640. Ignoring false positive.');
      return null;
    }
    else if (locatedZeroInd > 0) {
      console.log('AoB Match at ind ' + match.index
        + 'Has a ScreenID of Zero, but there is already a zero frameIndex at '
        + locatedZeroInd + '. Skipping the repeat.');
      return null;
    }
    else {
      locatedZeroInd = match.index;
    }
  }
  if (frameIndex == 5 && locatedZeroInd <= 0) {
    console.log('Screen 5 should not occur before 0. Ignoring false positive.');
      return null;
  }
  if (frameIndex == 15) {
    if (width != 1600 && height != 1600) {
      console.log('Width of Screen 15 is expected to be 1600. Ignoring false positive.');
      return null;
    }
  }
  
  var frameInitAddr = bytetools.decToHexInt(frameInitInd / 2);
  var frameInitIndex = frameInitInd;
  
  // For altering the width and height of a level:
  var indexWidth = frameInitInd + 14 + 2 + 4;
  var indexHeight = indexWidth + 14;
  
  // Grab the location of the virtualSizes by searching past the first two.
  var strRemainder = match[0];
  var strRemStartCut = strRemainder.indexOf(match[3]) + match[3].length;
  strRemainder = strRemainder.substring(strRemStartCut, match[0].length);
  
  //strRemainder now contains the bytes past frameID,Width,Height.
  var indexVWidth = frameInitIndex + strRemStartCut + strRemainder.indexOf(match[2]);
  var indexVHeight = frameInitIndex + strRemStartCut + strRemainder.indexOf(match[3]);
  indexVHeight = indexVHeight > 0 ? indexVHeight : undefined;
  indexVWidth = indexVWidth > 0 ? indexVWidth : undefined;

  var frameInitEndIndex = frameInitInd + fullData.substring(frameInitIndex).search(AOB_EndOfFunction);
  var frameInitEndAddr = bytetools.decToHexInt(frameInitEndIndex / 2);

  console.log('Frame Index start|end|length' + frameInitIndex + ' | ' + frameInitEndIndex + ' | ' + (frameInitEndIndex - frameInitIndex));

  var frameAOB = fullData.substring(frameInitIndex, frameInitEndIndex);
  
  return ({
    'frameInitAddr': frameInitAddr,
    'frameInitIndex': frameInitIndex,
    'frameInitEndIndex': frameInitEndIndex,
    'frameInitEndAddr': frameInitEndAddr,
    'frameIndexId': frameIndex,
    'width': width,
    'height': height,
    'virtualWidth': virtualWidth,
    'virtualHeight': virtualHeight,
    'widthIndex': indexWidth,
    'heightIndex': indexHeight,
    'virtualWidthIndex': indexVWidth,
    'virtualHeightIndex': indexVHeight,
    'frameAOB': frameAOB,
  });
}


function findAllGetInternalImageCalls(str, ind) {
  var re = AOB_getInternalImage;
  var match = [];
  var calls = [];
  while ((match = re.exec(str)) != null) {
    var call = getInternalImageIds(match[0], match.index + ind);
    // console.log('Match[0] ('+match[0]+'), match ind ('+match.index+') imageListChunkInd ('+ind+')');
    calls.push(call);
  }
  //
  return calls;
}

function getInternalImageIds(internalImgStr, indStringInFullData) {
  var imgIdHex = internalImgStr.substring(2, 10);
  var imgId = bytetools.hexIntToInt(imgIdHex);
  addrGetInternalImageRel = internalImgStr.substring(26, 34);
  var indexOffset = bytetools.hexIntToInt(addrGetInternalImageRel,true) * 2;
  var indInstructionAfterCall = indStringInFullData + internalImgStr.length;
  var indexGetInternalImage = indInstructionAfterCall + indexOffset;
  var addrGetInternalImage = bytetools.decToHexInt(indexGetInternalImage / 2);
  if (false)
  {
  console.log('internalImgStr.length: ' + internalImgStr.length);
  console.log('internalImgStr: ' + internalImgStr);
  console.log('Detected Index ('+indexGetInternalImage+') = IndexAfterCall ('+indInstructionAfterCall+') + IndexOffset ('+indexOffset+')');
  console.log('INDEX POSITION OF STRING ACCORDING TO SEARCH:' + fullData.search(internalImgStr));

  
  if (indexGetInternalImage == indexToLoadImage) {console.log('@@@@@HOLY CRAP IT WORKED BANANA@@@@@');}
  
  console.log('internalImgStr | indStringInFullData: ' + internalImgStr + ' | ' + indStringInFullData);
  console.log('Get Internal Image Index | Addr: ' + indexGetInternalImage + ' | ' + addrGetInternalImage);
  console.log("indexToLoadImage: " + indexToLoadImage);
  console.log("Distance from load image: " + (indexGetInternalImage - indexToLoadImage));
  console.log("Relative Index of Call: " + (indexOffset));
  console.log("Text from Relative Pos: " 
    + fullData.substring(indexGetInternalImage,indexGetInternalImage + 128));
    console.log("Text from KNOWN Pos: " 
    + fullData.substring(indexToLoadImage,indexToLoadImage + 128));
  console.log("--");
  }
  return ({
    'imgId': imgId,
    'imgIdHex': imgIdHex,
    'addrGetInternalImageRelative': addrGetInternalImageRel,
    'addrGetInternalImage': addrGetInternalImage,
    'indexGetInternalImage': indexGetInternalImage
  });
}

function mapLevelDataToImages(objs) {

  if (typeof objs == 'undefined') { objs = activeObjectMatches; }
  console.log('Number of objects to map to images:  ' + objs.length);
  if (imagesDone
    && imageAssets.length > 0
    && objs.length > 0) {
    for (var i = 0; i < objs.length; i++) {
      if (typeof objs[i].images != 'undefined'
        && objs[i].images.length > 0
        && objs[i].images[0] < imageAssets.length) {
        //console.log(activeObjectMatches[i].images[0]);
        objs[i].image = imageAssets[objs[i].images[0]].imgId;
        console.log('Mapped img for obj ' + i);
      }
      else if (typeof objs[i].images != 'undefined'
        && objs[i].images[0] >= imageAssets.length) {
        console.log('Attempted to map out-of-bounds image ID ' + objs[i].images[0] + ' Out of ' + imageAssets.length + ' from Active Object index ' + i);
      }
    }
  }
  return objs;
}

function createPath(path) {
  try {
    fs.accessSync(path, fs.F_OK);
  } catch (e) {
    console.log('Could not find path "' + path + '". Attempting to create it.');
    fs.mkdirSync(path);
  }

}

function findImgAssetById(imgId) {
  console.log('Not implemented.');
  return;

  var result;
  for (var ia of imageAssets) {
    // Implement if needed.
  }
  return result;
}


function findDV1(lvlStr, strInd) {
  fs.readFile('../data/dictionary/levels-1-20-4.dict', null, function (err, fileData) {
    var dvMatches = [];
    var lvlDict = JSON.parse(fileData);

    for (l of lvlDict.levels) {
      var i = bytetools.decToHexInt(l.screenID);
      var w = bytetools.decToHexInt(l.width);
      var h = bytetools.decToHexInt(l.height);
      var vw = bytetools.decToHexInt(l.virtualWidth);
      var vh = bytetools.decToHexInt(l.virtualHeight);

      var wildHex = '[0-9a-f]{2,50}';
      // 14000000c74608e0390000c7460c480700006a00c74610e03900006a00c7461448070000
      
      var targetAOB = new RegExp(i + wildHex + w + wildHex + h + wildHex + vw + wildHex + vh);
      console.log(lvlStr.length);
      console.log(targetAOB);
      dvMatches = targetAOB.exec(lvlStr);

    }
    if (dvMatches[0].length > 1024) { throw ('This seems supiciously long \n' + dvMatches[0].substring(0, 1024)) }
    console.log('@@@@@@@@@@@@@');
    console.log('@@Callback from findDV1@@');
    console.log(dvMatches.length);
    console.log('Addr index: ' + strInd + dvMatches.index + strInd);
    console.log('@@@@@@@@@@@@@');
    console.log('--Snippet--\n' + dvMatches[0]);
    console.log('@@@@@@@@@@@@@');
    throw 'BORK';
  });
}

function findALLFRAMES(lvlStr, strInd) {
  fs.readFile('../data/dictionary/levels-1-20-4.dict', null, function (err, fileData) {
    var dvMatches = [];
    var lvlDict = JSON.parse(fileData);

    for (l of lvlDict.levels) {
      var i = bytetools.decToHexInt(l.screenID);
      var w = bytetools.decToHexInt(l.width);
      var h = bytetools.decToHexInt(l.height);
      var vw = bytetools.decToHexInt(l.virtualWidth);
      var vh = bytetools.decToHexInt(l.virtualHeight);
      
      //var wildHex = '[0-9a-f]{2,50}';
      // 14000000c74608e0390000c7460c480700006a00c74610e03900006a00c7461448070000
      
      //var targetAOB = new RegExp( i + wildHex + w + wildHex + h + wildHex + vw + wildHex + vh);
      
      var targetAOB = AOB_FrameInit;
      console.log(lvlStr.length);
      console.log(targetAOB);
      dvMatches = targetAOB.exec(lvlStr);
      var finds = [];
      var locatedZeroInd = -1;

      while ((dvMatches = targetAOB.exec(lvlStr)) != null) {
        var sid = bytetools.hexIntToInt(dvMatches[1]);
        var width = bytetools.hexIntToInt(dvMatches[2]);
        if (sid >= 100) { continue; }
        if (sid == 0) {
          if (width != 640) {
            console.log('Width of Screen 0 is expected to be 640. Ignoring false positive.');
            continue;
          }
          else if (locatedZeroInd > 0) {
            console.log('AoB Match at ind ' + dvMatches.index
              + 'Has a ScreenID of Zero, but there is already a zero sID at '
              + locatedZeroInd + '. Skipping the repeat.');
            continue;
          }
          else {
            locatedZeroInd = dvMatches.index;
          }

        }
        if (sid == 5 && locatedZeroInd <= 0) {
          console.log('Screen 5 should not occur before 0. Ignoring false positive.');
            continue;
        }
        if (sid == 15) {
          if (width != 1600) {
            console.log('Width of Screen 15 is expected to be 1600. Ignoring false positive.');
            continue;
          }
        }
        var find = {
          "ind": (dvMatches.index + strInd),
          "screenID": sid,
          "MiniAOB": fullData.substring(dvMatches.index + strInd, dvMatches.index + strInd + 64)
        };
        finds.push(find);
      }

    }
    //if (dvMatches[0].length > 1024) { throw ('This seems supiciously long \n' + dvMatches[0].substring(0, 1024)); }
    console.log('@@@@@@@@@@@@@');
    console.log('@@Callback from findALLFRAMES@@');
    console.log(finds);
    console.log(finds.length);
    //console.log('Addr index: ' + strInd + dvMatches.index + strInd);
    console.log('@@@@@@@@@@@@@');
    //console.log('--Snippet--\n' + dvMatches[0]);
    console.log('@@@@@@@@@@@@@');
    throw 'BORK';
  });
}

main();
